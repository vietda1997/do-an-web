<%-- 
    Document   : home
    Created on : Nov 25, 2017, 11:26:05 PM
    Author     : Viet Diep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<form action="?p=1" method="post" name="users">

    <h2>Users</h2>
    <div class="leftmanagefunction">
        <select name="action" size="1" id="selectpermission">
            <option selected="selected">Actions</option>
            <option value="lock">Lock</option>
            <option value="unlock">Unlock</option>
            <option value="delete">Delete</option>
        </select>
    </div>
    <div class="rightmanagefunction">
        Keyword:
        <s:textfield name="keyword"/>
        |
        Sort:
        <s:select list="listSortBy" listKey="key" listValue="value" name="sortBy"/>
        <s:select list="listOrderBy" listKey="key" listValue="value" name="orderBy"/>
        | Filter:
        <s:select list="listPermission" name="filterPermission"/>
        <input class="buttonfunction" type="submit" value="Filter" />
    </div>
    <div class="clear"></div>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table managetable-row-showaction">

        <tr>
            <td class="head checkselect" width="10px"><input onchange="toggleCheckAll(document.users.userids, document.users.checkAll, this.checked);" name="checkAll" type="checkbox" value="" /></td>
            <td class="head" width="150px">Username</td>
            <td class="head">Permission</td>
            <td class="head" width="150px">Email</td>
            <td class="head" width="100px">Created Date</td>
            <td class="head" width="80px">Status</td>
        </tr>
        
        <s:iterator value="userViewer">
            <tr>
                <td class="checkselect"><s:checkbox onchange="unCheckCheckAll(document.users.checkAll, this)" theme="simple" name="userids" fieldValue="%{id}"/></td>
                <td class="maintext">
                    ${userName}
                    <br/>
                    <div class="doactionacticles">
                        <s:url action="edituser" namespace="/backend" var="linkEdit">
                        <s:param name="userid">${id}</s:param>
                    </s:url>
                    <a href="${linkEdit}" title="Edit User">Edit</a>
                    </div>
                </td>
                <td>${role}</td>
                <td>${email}</td>
                <td>${createDTM}</td>
                <td>
                    <s:if test="isLock == 1">
                        Locked
                    </s:if>
                    <s:else>
                        Available
                    </s:else>
                </td>
            </tr>
        </s:iterator>
     
        <s:if test="objPaging.countDisplay == 1">
            <s:hidden theme="simple" name="userids"/>
        </s:if>
        <tr>
            <td class="head checkselect" width="10px"><input onchange="toggleCheckAll(document.users.userids, document.users.checkAll, this.checked);" name="checkAll" type="checkbox" value="" /></td>
            <td class="head">Username</td>
            <td class="head">Permission</td>
            <td class="head">Email</td>
            <td class="head">Created Date</td>
            <td class="head">Status</td>
        </tr>

    </table>
    
</form>
