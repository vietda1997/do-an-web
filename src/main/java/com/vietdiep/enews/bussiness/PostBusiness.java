/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.bussiness;

import com.vietdiep.enews.model.Post;
import java.sql.Connection;
import com.vietdiep.enews.database.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author vietda
 */
public class PostBusiness {

    private static PostBusiness instance = new PostBusiness();

    private static final Logger _logger = Logger.getLogger(PostBusiness.class);

    public static PostBusiness getIntance() {
        return instance;
    }

    public Post getHotPostTop1() {
        Post post = null;
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            String string = "SELECT * FROM POST WHERE isHotTop1 = 1 AND STATUS = 'PS_published' ORDER BY createDTM DESC LIMIT 1";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    post = new Post();
                    post.setPostID(resultSet.getInt("postID"));
                    post.setCategoryID(resultSet.getInt("categoryID"));
                    post.setUserID(resultSet.getInt("userID"));
                    post.setCreatedDTM(resultSet.getLong("createDTM"));
                    post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                    post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                    post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                    post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                    post.setTitle(resultSet.getString("title"));
                    post.setShortDescription(resultSet.getString("shortDescription"));
                    post.setContent(resultSet.getString("content"));
                    post.setAvatarImage(resultSet.getString("images"));
                    post.setStatus(resultSet.getString("status"));
                    post.setViews(resultSet.getInt("view"));
                    post.setPublishedTime(resultSet.getLong("publishedTime"));
                }
            } catch (SQLException ex) {
                _logger.error(ex.getMessage());
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return post;
    }

    public List<Post> getHotPostTop234() {
        List<Post> hotPosts = new ArrayList<>();
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            String string = "SELECT * FROM POST WHERE isHotTop234 = 1 AND STATUS = 'PS_published' ORDER BY createDTM DESC LIMIT 3";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setPostID(resultSet.getInt("postID"));
                    post.setCategoryID(resultSet.getInt("categoryID"));
                    post.setUserID(resultSet.getInt("userID"));
                    post.setCreatedDTM(resultSet.getLong("createDTM"));
                    post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                    post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                    post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                    post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                    post.setTitle(resultSet.getString("title"));
                    post.setShortDescription(resultSet.getString("shortDescription"));
                    post.setContent(resultSet.getString("content"));
                    post.setAvatarImage(resultSet.getString("images"));
                    post.setStatus(resultSet.getString("status"));
                    post.setViews(resultSet.getInt("view"));
                    post.setPublishedTime(resultSet.getLong("publishedTime"));
                    hotPosts.add(post);
                }
            } catch (SQLException ex) {
                _logger.error(ex.getMessage());
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return hotPosts;
    }

    public List<Post> getPostsPublishedByCategory(int categoryID) {
        List<Post> hotPosts = new ArrayList<>();
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            String string = "SELECT * FROM POST WHERE categoryID = ? AND status = 'PS_Published' ORDER BY createDTM DESC";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                statement.setInt(1, categoryID);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setPostID(resultSet.getInt("postID"));
                    post.setCategoryID(resultSet.getInt("categoryID"));
                    post.setUserID(resultSet.getInt("userID"));
                    post.setCreatedDTM(resultSet.getLong("createDTM"));
                    post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                    post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                    post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                    post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                    post.setTitle(resultSet.getString("title"));
                    post.setShortDescription(resultSet.getString("shortDescription"));
                    post.setContent(resultSet.getString("content"));
                    post.setAvatarImage(resultSet.getString("images"));
                    post.setStatus(resultSet.getString("status"));
                    post.setViews(resultSet.getInt("view"));
                    post.setPublishedTime(resultSet.getLong("publishedTime"));
                    hotPosts.add(post);
                }
            } catch (SQLException ex) {
                _logger.error(ex.getMessage());
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return hotPosts;
    }

    public Post getPost(int postId) {
        Connection connection = DBConnection.getInstance().getConnection();
        Post post = new Post();
        if (connection != null) {
            String string = "SELECT * FROM POST WHERE postID = ?";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                statement.setInt(1, postId);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    post.setPostID(resultSet.getInt("postID"));
                    post.setCategoryID(resultSet.getInt("categoryID"));
                    post.setUserID(resultSet.getInt("userID"));
                    post.setCreatedDTM(resultSet.getLong("createDTM"));
                    post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                    post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                    post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                    post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                    post.setTitle(resultSet.getString("title"));
                    post.setShortDescription(resultSet.getString("shortDescription"));
                    post.setContent(resultSet.getString("content"));
                    post.setAvatarImage(resultSet.getString("images"));
                    post.setStatus(resultSet.getString("status"));
                    post.setViews(resultSet.getInt("view"));
                    post.setPublishedTime(resultSet.getLong("publishedTime"));
                }
            } catch (SQLException ex) {
                _logger.error(ex.getMessage());
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return post;
    }

    public List<Post> searchPost(String keyWord, long from, long to, int categoryId) throws SQLException {
        List<Post> posts = new ArrayList<>();
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            PreparedStatement statement;
            StringBuilder string = new StringBuilder("SELECT * FROM POST WHERE status = 'PS_Published'");
            int pos = 1;
            if(!keyWord.equalsIgnoreCase("")){
                string.append(" AND title LIKE ?");
            }
            if(from == 0 && to != 0){
                string.append(" AND publishedTime <= ?");
            } else if(from != 0 && to == 0){
                string.append(" AND publishedTime >= ?");
            } else if(from != 0 && to != 0){
                string.append(" AND publishedTime >= ? AND publishedTime <= ?");
            }
            if(categoryId != 0){
                string.append(" AND categoryID = ?");
            }
            String query = string.toString();
            statement = connection.prepareStatement(query);
            if(!keyWord.equalsIgnoreCase("")){
                statement.setString(pos++, "%" + keyWord + "%");
            }
            if(from == 0 && to != 0){
                statement.setLong(pos++, to);
            } else if(from != 0 && to == 0){
                statement.setLong(pos++, from);
            } else if (from != 0 && to != 0) {
                statement.setLong(pos++, from);
                statement.setLong(pos++, to);
            }
            if(categoryId != 0){
                statement.setInt(pos++, categoryId);
            }
            try {
                
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setPostID(resultSet.getInt("postID"));
                    post.setCategoryID(resultSet.getInt("categoryID"));
                    post.setUserID(resultSet.getInt("userID"));
                    post.setCreatedDTM(resultSet.getLong("createDTM"));
                    post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                    post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                    post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                    post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                    post.setTitle(resultSet.getString("title"));
                    post.setShortDescription(resultSet.getString("shortDescription"));
                    post.setContent(resultSet.getString("content"));
                    post.setAvatarImage(resultSet.getString("images"));
                    post.setStatus(resultSet.getString("status"));
                    post.setViews(resultSet.getInt("view"));
                    post.setPublishedTime(resultSet.getLong("publishedTime"));
                    posts.add(post);
                }
            } catch (SQLException ex) {
                _logger.error(ex.getMessage());
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return posts;
    }

    public List<Post> getLastedPublishedHightlightPosts() {
        List<Post> posts = null;
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection == null) {
            return null;
        }
        posts = new ArrayList<>();
        String string = "SELECT * FROM POST WHERE status = 'PS_Published' and isHightlight = 1 ORDER BY publishedTime DESC LIMIT 15";
        try {
            PreparedStatement statement = connection.prepareStatement(string);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Post post = new Post();
                post.setPostID(resultSet.getInt("postID"));
                post.setCategoryID(resultSet.getInt("categoryID"));
                post.setUserID(resultSet.getInt("userID"));
                post.setCreatedDTM(resultSet.getLong("createDTM"));
                post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                post.setTitle(resultSet.getString("title"));
                post.setShortDescription(resultSet.getString("shortDescription"));
                post.setContent(resultSet.getString("content"));
                post.setAvatarImage(resultSet.getString("images"));
                post.setStatus(resultSet.getString("status"));
                post.setViews(resultSet.getInt("view"));
                post.setPublishedTime(resultSet.getLong("publishedTime"));
                posts.add(post);
            }
        } catch (SQLException ex) {
            _logger.error(ex.getMessage());
        } finally {
            DBConnection.getInstance().returnConnectionToPool(connection);
        }
        return posts;
    }

    public List<Post> getHighViewsPublishedPost() {
        List<Post> posts = null;
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection == null) {
            return null;
        }
        posts = new ArrayList<>();
        String string = "SELECT * FROM POST WHERE STATUS = 'PS_Published' ORDER BY view DESC LIMIT 15";
        try {
            PreparedStatement statement = connection.prepareStatement(string);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Post post = new Post();
                post.setPostID(resultSet.getInt("postID"));
                post.setCategoryID(resultSet.getInt("categoryID"));
                post.setUserID(resultSet.getInt("userID"));
                post.setCreatedDTM(resultSet.getLong("createDTM"));
                post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                post.setTitle(resultSet.getString("title"));
                post.setShortDescription(resultSet.getString("shortDescription"));
                post.setContent(resultSet.getString("content"));
                post.setAvatarImage(resultSet.getString("images"));
                post.setStatus(resultSet.getString("status"));
                post.setViews(resultSet.getInt("view"));
                post.setPublishedTime(resultSet.getLong("publishedTime"));
                posts.add(post);
            }
        } catch (SQLException ex) {
            _logger.error(ex.getMessage());
        } finally {
            DBConnection.getInstance().returnConnectionToPool(connection);
        }
        return posts;
    }

    // get count post pending
    public long countPostPublished() {
        Connection connection = DBConnection.getInstance().getConnection();
        int count = 0;
        if (connection != null) {
            String string = "SELECT COUNT(postID) FROM POST WHERE STATUS = 'PS_Published'";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    count = rs.getInt(1);
                }
                return count;
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(PostBusiness.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public long countPostPending() {
        Connection connection = DBConnection.getInstance().getConnection();
        int count = 0;
        if (connection != null) {
            String string = "SELECT COUNT(postID) FROM POST WHERE STATUS = 'PS_Pending'";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    count = rs.getInt(1);
                }
                return count;
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(PostBusiness.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public long countPostRejected() {
        Connection connection = DBConnection.getInstance().getConnection();
        int count = 0;
        if (connection != null) {
            String string = "SELECT COUNT(postID) FROM POST WHERE STATUS = 'PS_Reject'";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    count = rs.getInt(1);
                }
                return count;
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(PostBusiness.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public long countAllPost() {
        Connection connection = DBConnection.getInstance().getConnection();
        int count = 0;
        if (connection != null) {
            String string = "SELECT COUNT(postID) FROM POST ";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    count = rs.getInt(1);
                }
                return count;
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(PostBusiness.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public List<Post> getRecentPostPending() {
        List<Post> listPostRecentPending = new ArrayList<>();
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            String string = "SELECT * FROM POST WHERE status = 'PS_Pending' ORDER BY createDTM DESC LIMIT 15";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setPostID(resultSet.getInt("postID"));
                    post.setCategoryID(resultSet.getInt("categoryID"));
                    post.setUserID(resultSet.getInt("userID"));
                    post.setCreatedDTM(resultSet.getLong("createDTM"));
                    post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                    post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                    post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                    post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                    post.setTitle(resultSet.getString("title"));
                    post.setShortDescription(resultSet.getString("shortDescription"));
                    post.setContent(resultSet.getString("content"));
                    post.setAvatarImage(resultSet.getString("images"));
                    post.setStatus(resultSet.getString("status"));
                    post.setViews(resultSet.getInt("view"));
                    post.setPublishedTime(resultSet.getLong("publishedTime"));
                    listPostRecentPending.add(post);
                }
            } catch (SQLException ex) {
                _logger.error(ex.getMessage());
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return listPostRecentPending;
    }

    public List<Post> getRecentPostPublished() {
        List<Post> listPostRecentPublished = new ArrayList<>();
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            String string = "SELECT * FROM POST WHERE status = 'PS_Published' ORDER BY createDTM DESC LIMIT 15";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setPostID(resultSet.getInt("postID"));
                    post.setCategoryID(resultSet.getInt("categoryID"));
                    post.setUserID(resultSet.getInt("userID"));
                    post.setCreatedDTM(resultSet.getLong("createDTM"));
                    post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                    post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                    post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                    post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                    post.setTitle(resultSet.getString("title"));
                    post.setShortDescription(resultSet.getString("shortDescription"));
                    post.setContent(resultSet.getString("content"));
                    post.setAvatarImage(resultSet.getString("images"));
                    post.setStatus(resultSet.getString("status"));
                    post.setViews(resultSet.getInt("view"));
                    post.setPublishedTime(resultSet.getLong("publishedTime"));
                    listPostRecentPublished.add(post);
                }
            } catch (SQLException ex) {
                _logger.error(ex.getMessage());
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return listPostRecentPublished;
    }

    public List<Post> getRecentPostRejected() {
        List<Post> listPostRecentRejected = new ArrayList<>();
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            String string = "SELECT * FROM POST WHERE status = 'PS_Reject' ORDER BY createDTM DESC LIMIT 15";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setPostID(resultSet.getInt("postID"));
                    post.setCategoryID(resultSet.getInt("categoryID"));
                    post.setUserID(resultSet.getInt("userID"));
                    post.setCreatedDTM(resultSet.getLong("createDTM"));
                    post.setUpdatedDTM(resultSet.getLong("updateDTM"));
                    post.setIsHotTop1(resultSet.getBoolean("isHotTop1"));
                    post.setIsHotTop234(resultSet.getBoolean("isHotTop234"));
                    post.setIsHightlight(resultSet.getBoolean("isHightlight"));
                    post.setTitle(resultSet.getString("title"));
                    post.setShortDescription(resultSet.getString("shortDescription"));
                    post.setContent(resultSet.getString("content"));
                    post.setAvatarImage(resultSet.getString("images"));
                    post.setStatus(resultSet.getString("status"));
                    post.setViews(resultSet.getInt("view"));
                    post.setPublishedTime(resultSet.getLong("publishedTime"));
                    listPostRecentRejected.add(post);
                }
            } catch (SQLException ex) {
                _logger.error(ex.getMessage());
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return listPostRecentRejected;
    }

}
