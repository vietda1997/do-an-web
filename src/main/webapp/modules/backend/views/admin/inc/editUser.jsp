<%-- 
    Document   : addUser
    Created on : Nov 22, 2017, 9:23:03 AM
    Author     : Viet Diep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<h2>
        Edit User
</h2>

<s:if test="actionMessages.size() > 0">
    <div class="success">
        <s:property value="actionMessages.get(0)" />
    </div>
</s:if>

<form action="edituser" method="post">
    <div class="boxborder">
        <h4 class="blocksubhead">Personal Information</h4>
        <div class="blockrow-first">
            <label>Username (<font color="red">*</font>):</label>
                <s:if test="user.id != null">
                    <s:hidden name="user.id"/>
                </s:if>
                <s:textfield theme="simple" name="userName"/>
        </div>
        <div class="blockrow-content">
            <p><label>Password <s:if test="user == null || user.id == null">(<font color="red">*</font>)</s:if>:</label><s:password theme="simple" name="password"/></p>
                <p>
                    <label>Password Again <s:if test="user == null || user.id == null">(<font color="red">*</font>)</s:if>:</label><s:password theme="simple" name="passwordAgain"/>

                    <span class="errorMessage"><s:property value="fieldErrors.get('invPassword').get(0)" /></span>
            </p>
        </div>

        <div class="blockrow-content">
            <p><label>Email (<font color="red">*</font>):</label><s:textfield theme="simple" name="email"/></p>
            <p>
                <label>Email Again (<font color="red">*</font>):</label><s:textfield theme="simple" name="emailAgain"/>
                <span class="errorMessage"><s:property value="fieldErrors.get('invEmail').get(0)" /></span>
            </p>

        </div>

        <h4 class="blocksubhead">Permission</h4>

        <div class="blockrow-first">
            <label>Permission (<font color="red">*</font>):</label>
                <s:select list="listPermission" name="permissionChoose" />

            <span class="errorMessage"><s:property value="fieldErrors.get('invPermission').get(0)" /></span>
        </div>

        <h4 class="blocksubhead">Additional Information</h4>
        <div class="blockrow-first">
            <p><label>Full Name (<font color="red">*</font>):</label>
                    <s:textfield name="fullName"/>
                <span class="errorMessage"><s:property value="fieldErrors.get('invFullname').get(0)" /></span>
            </p>
        </div>
        <div class="blockrow-content">
            <p><label>Address (<font color="red">*</font>):</label><s:textfield name="address"/>
                <span class="errorMessage"><s:property value="fieldErrors.get('invAddress').get(0)" /></span>
            </p>
        </div>
        <div class="blockrow-content">
            <p><label>Phone Number (<font color="red">*</font>):</label><s:textfield name="phoneNumber"/>
                <span class="errorMessage"><s:property value="fieldErrors.get('invPhonenumber').get(0)" /></span>
            </p>
        </div>

        <div class="blockrow-content">
            <s:radio label="Status" name="status" list="#{'0':'Active','1':'Blocked'}" value="isLock" />
        </div>
        
        <div class="blockrow-content">
            <div class="blockrow-right">
                <s:if test="user.id != null">
                    <input type="submit" value="Update"/>
                </s:if>
                <s:else>
                    <input type="submit" value="Update"/>
                </s:else>
                <input type="reset" value="Reset"/>
            </div>
        </div>
        
    </div>
</form>
