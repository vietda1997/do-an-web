<%-- 
    Document   : newspage
    Created on : Oct 28, 2017, 10:34:42 AM
    Author     : Viet Diep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="content">
    <div id="left" class="boxborder"><jsp:include page="inc/left.jsp"/></div>
    <div id="middle"><jsp:include page="../categorypage/inc/middle.jsp"/></div>
    <div id="right"><jsp:include page="../common/advertisement.jsp"/></div>
</div>