<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<h2>
        Add Category
</h2>

<s:if test="actionMessages.size() > 0">
    <div class="success">
        <s:property value="actionMessages.get(0)" />
    </div>
</s:if>
<form action="" method="post">
    <div class="boxborder">
        <h4 class="blocksubhead">Category Information</h4>
        <form action="" method="post">
            <div class="blockrow-first">
                <label>Name (<font color="red">*</font>):</label>
                <s:if test="category.categoryid != null">
                    <s:hidden name="category.categoryid"/>
                </s:if>
                <s:textfield theme="simple" name="category.categoryName"/>
                <span class="errorMessage"><s:property value="fieldErrors.get('invCategoryName').get(0)"/></span>
            </div>

            <s:if test="(category == null || category.categoryid == null) || category.childrens.size() == 0">
                <div class="blockrow-content">
                    <p><label>Parent:</label>
                        <s:select name="parentCategoriesChoose" list="parentCategories" theme="simple" listKey="categoryid" listValue="categoryName" size="1" style="width: 200px"/>
                        <span class="errorMessage"><s:property value="fieldErrors.get('invParentCategories').get(0)" /></span></p>
                </div>
            </s:if>
            <div class="blockrow-content">
                <p><label>Order (<font color="red">*</font>):</label>
                    <s:textfield theme="simple" name="category.orderNumber" cssClass="ordertext"/>
                    <span class="errorMessage"><s:property value="fieldErrors.get('invOrderNumber').get(0)" /></span>
                </p>
            </div>

            <div class="blockrow-content">
                <p><label>Description (<font color="red">*</font>):</label>
                    <s:textarea cols="50" rows="5" theme="simple" name="category.description"></s:textarea>
                    <span class="errorMessage"><s:property value="fieldErrors.get('invDescription').get(0)" /></span>
                </p>
            </div>

            <div class="blockrow-content">
                <p><label>Editor:</label>
                    <s:select name="editorsChoose" list="editors" theme="simple" listKey="userid" listValue="username" size="1" style="width: 200px"/>
                    <span class="errorMessage"><s:property value="fieldErrors.get('invEditors').get(0)" /></span>
                </p>
                <div class="blockrow-right">
                    Choose editor for this category
                </div>
            </div>

            <div class="blockrow-content">
                <p><label>Reporter:</label>
                    <s:hidden name="reporterhidden"/>
                    <span id="reporter"></span>
                    <script type="text/javascript">
                        linkEditUser = '<s:url action="profile" namespace="/backend"/>';
                        var hidden = document.getElementById('reporterhidden');
                        var span = document.getElementById('reporter');
                        <s:iterator value="category.reporters" status="row">
                            addUser(hidden, '${userid}', '${username}');
                        </s:iterator>
                            validateUserSpan(hidden, span);
                    </script>
                </p>
                <div class="blockrow-right">
                    <s:url action="selectusers" namespace="/backend" var="linkSelect">
                    </s:url>
                    <p><input onclick="poptastic('${linkSelect}?perm=reporter', '1024', '700')" class="buttonfunction" type="button" value="Add" /></p>
                    <p>Choose reporter for this category.</p>
                </div>
            </div>

            <div class="blockrow-content" id="editorchoose">
                <p><label>Comment Approver:</label>
                    <s:hidden name="commenthidden"/>
                    <span id="comment"></span>
                    <script type="text/javascript">
                        var hidden = document.getElementById('commenthidden');
                        var span = document.getElementById('comment');
                        <s:iterator value="category.commentapprovers" status="row">
                            addUser(hidden, '${userid}', '${username}');
                        </s:iterator>
                            validateUserSpan(hidden, span);
                    </script>
                </p>
                
                <div class="blockrow-right">
                    <p><input class="buttonfunction" onclick="poptastic('${linkSelect}?perm=comment', '1024', '700')" type="button" value="Add" /></p>
                    <p>Add comment approver for this category</p>
                </div>
            </div>
            <div class="blockrow-content">
                <div class="blockrow-right">
                    <s:if test="category.categoryid != null">
                        <input type="submit" value="Update"/>
                    </s:if>
                    <s:else>
                        <input type="submit" value="Add"/>
                    </s:else>
                    <input type="reset" value="Reset"/>
                </div>
            </div>
        </form>
    </div>
</form>