/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.action.editorAction;

import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.bussiness.CategoryBusiness;
import com.vietdiep.enews.bussiness.PostBusiness;
import com.vietdiep.enews.bussiness.UserBusiness;
import com.vietdiep.enews.model.Category;
import com.vietdiep.enews.model.Post;
import com.vietdiep.enews.model.User;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;

/**
 *
 * @author vietda
 */
public class DashBoardAction extends ActionSupport implements ServletRequestAware{

    private HttpServletRequest request;
    private List<Category> categories;
    private List<User> reporters;
    private Long countAll;
    private Long countPending;
    private Long countPublished;
    private Long countDraft;
    private List<Post> recentPublished;
    private List<Post> recentPending;
    private List<Post> recentDrafts;
    private String requestMeta = "";
    
    @Override
    public String execute(){
        
        //this.request.setAttribute("actionName", "dashboard");
        this.requestMeta = "dashboard";
        this.categories = CategoryBusiness.getInstance().getListAllCategory();
        this.reporters = UserBusiness.instance.getListUserByRole("reporter");
        this.countPending = PostBusiness.getIntance().countPostPending();
        this.countAll = PostBusiness.getIntance().countAllPost();
        this.countDraft = PostBusiness.getIntance().countPostRejected();
        this.countPublished = PostBusiness.getIntance().countPostPublished();
        this.recentPending = PostBusiness.getIntance().getRecentPostPending();
        this.recentPublished = PostBusiness.getIntance().getRecentPostPublished();
        this.recentDrafts = PostBusiness.getIntance().getRecentPostRejected();
        return SUCCESS;
    }
    
    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.request = hsr;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<User> getReporters() {
        return reporters;
    }

    public void setReporters(List<User> reporters) {
        this.reporters = reporters;
    }

    public Long getCountAll() {
        return countAll;
    }

    public void setCountAll(Long countAll) {
        this.countAll = countAll;
    }

    public Long getCountPending() {
        return countPending;
    }

    public void setCountPending(Long countPending) {
        this.countPending = countPending;
    }

    public Long getCountPublished() {
        return countPublished;
    }

    public void setCountPublished(Long countPublished) {
        this.countPublished = countPublished;
    }

    public Long getCountDraft() {
        return countDraft;
    }

    public void setCountDraft(Long countDraft) {
        this.countDraft = countDraft;
    }

    public List<Post> getRecentPublished() {
        return recentPublished;
    }

    public void setRecentPublished(List<Post> recentPublished) {
        this.recentPublished = recentPublished;
    }

    public List<Post> getRecentPending() {
        return recentPending;
    }

    public void setRecentPending(List<Post> recentPending) {
        this.recentPending = recentPending;
    }

    public List<Post> getRecentDrafts() {
        return recentDrafts;
    }

    public void setRecentDrafts(List<Post> recentDrafts) {
        this.recentDrafts = recentDrafts;
    }

    public String getRequestMeta() {
        return requestMeta;
    }

    public void setRequestMeta(String requestMeta) {
        this.requestMeta = requestMeta;
    }
    
}
