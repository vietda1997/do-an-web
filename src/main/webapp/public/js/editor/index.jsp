<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Replace Textarea by Code - CKEditor Sample</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
	<script type="text/javascript" src="ckeditor.js"></script>
	<link href="sample.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%
	out.print(request.getParameter("editor1"));
%>
	<h1>
		CKEditor Sample
	</h1>
	<!-- This <div> holds alert messages to be display in the sample page. -->
	<div id="alerts">
		<noscript>
			<p>
				<strong>CKEditor requires JavaScript to run</strong>. In a browser with no JavaScript
				support, like yours, you should still see the contents (HTML data) and you should
				be able to edit it normally, without a rich editor interface.
			</p>
		</noscript>
	</div>
	<form action="" method="post">
		<p>
			<label for="editor1">
				Editor 1:</label><br />
			<textarea cols="80" id="editor1" name="editor1" rows="10">Thằng Quang khùng</textarea>
			<script type="text/javascript">
			//<![CDATA[
				CKEDITOR.replace( 'editor1' );

			//]]>
			</script>
		</p>
		<p>
			<label for="editor2">
				Editor 2:</label><br />
			<textarea cols="80" id="editor2" name="editor2" rows="10">
            <%=request.getParameter("editor2")%>;
            </textarea>
		</p>
		<p>
			<input type="submit" value="Submit" />
		</p>
	</form>
	<div id="footer">
		<hr />
		<p>
			CKEditor - The text editor for Internet - <a href="http://ckeditor.com/">http://ckeditor.com</a>
		</p>
		<p id="copy">
			Copyright &copy; 2003-2010, <a href="http://cksource.com/">CKSource</a> - Frederico
			Knabben. All rights reserved.
		</p>
	</div>
</body>
</html>
