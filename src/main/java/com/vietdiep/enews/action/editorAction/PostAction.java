/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.action.editorAction;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.model.Category;
import com.vietdiep.enews.model.Post;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.vietdiep.enews.utils.KeyValue;
import org.apache.struts2.interceptor.ServletRequestAware;

/**
 *
 * @author vietda
 */
public class PostAction extends ActionSupport implements ServletRequestAware{
    
    private HttpServletRequest request;
    //Form
    private String filter;
    private String keyword;
    private Long[] postids;
    private String[] action;
    private String sortBy;
    private String orderBy;
    private List<KeyValue> listSortBy = Arrays.asList(
            new KeyValue[]{new KeyValue("lastModified", "Date"), new KeyValue("totalPageView", "View")});
    private List<KeyValue> listOrderBy = Arrays.asList(
            new KeyValue[]{new KeyValue("desc", "Descending"), new KeyValue("asc", "Ascending")});
    private List<String> listFilterPost = Arrays.asList(
            new String[]{"All Post", "Hot", "Highlights"});
    private String filterPost;
    private List<Category> categories;
    private Long categoryChoose;    
    private Long countAll;
    private Long countPending;
    private Long countPublished;
    private Long countDraft;
    private Long countTrash;

     @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.request = hsr;
    }
    
    @Override
    public String execute() throws Exception {

//        request.setAttribute("actionName", "posts");
//        //Filter
//        filter = request.getParameter("t");
//        if (filter == null) {
//            filter = "all";
//        }
//
//        //Set list categories
//        categories = new ArrayList<Category>();
//        Category cate = new Category();
//        cate.setCategoryName("All Categories");
//        categories.add(cate);
//        categories.addAll(Format.getInstance().getFormatCategories(editor.getCategoriesEditor()));
//
//        //Set categories filter
//        Set<Category> categoriesFilter;
//        if (this.categoryChoose == null) {
//            categoriesFilter = editor.getCategoriesEditor();
//        } else {
//            Category cate1 = new Category();
//            cate1.setCategoryid(categoryChoose);
//            categoriesFilter = new HashSet<Category>();
//            categoriesFilter.add(cate1);
//        }
//
//        /**
//         * Process Actions
//         */
//        if (action != null) {
//            if (action[0].equals("approve") || action[1].equals("approve")) {
//                bs.approvePosts(postids, editor);
//                addActionMessage("Approve successfully!");
//            } else if (action[0].equals("trash") || action[1].equals("trash")) {
//                bs.setTrash(postids, true);
//                addActionMessage("Move to trash successfully!");
//            } else if (action[0].equals("restore") || action[1].equals("restore")) {
//                bs.setTrash(postids, false);
//                addActionMessage("Restore successfully!");
//            } else if (action[0].equals("deletepermanently") || action[1].equals("deletepermanently")) {
//                bs.deletePermanently(postids);
//                addActionMessage("Delete successfully!");
//            }
//            postids = null;
//        }
//        /**
//         * End Process Actions
//         */
//        /**
//         * Default value
//         */
//        if (sortBy == null) {
//            sortBy = listSortBy.get(0).getKey();
//        }
//        if (orderBy == null) {
//            orderBy = listOrderBy.get(0).getKey();
//        }
//        if (filterPost == null || filterPost.equals("")) {
//            filterPost = listFilterPost.get(0);
//        }
//        /**
//         * End default value
//         */
//        //Get count
//        this.countAll = bs.getTotalPostsByCategories("", "all", editor.getUserid(), editor.getCategoriesEditor(), null);
//        this.countPending = bs.getTotalPostsByCategories("","pending", editor.getUserid(), editor.getCategoriesEditor(), null);
//        this.countPublished = bs.getTotalPostsByCategories("","published", editor.getUserid(), editor.getCategoriesEditor(), null);
//        this.countDraft = bs.getTotalPostsByCategories("","draft", editor.getUserid(), editor.getCategoriesEditor(), null);
//        this.countTrash = bs.getTotalPostsByCategories("","trash", editor.getUserid(), editor.getCategoriesEditor(), null);
//
//        /**
//         * Paging
//         */
//        int pageSize = Integer.parseInt(this.getText("PageSize"));
//        int page = Format.getInstance().parsePage(request.getParameter("p"));
//        long totalRecord = bs.getTotalPostsByCategories(filterPost, filter, editor.getUserid(), categoriesFilter, keyword);
//        if (page > Math.ceil(totalRecord / (double) pageSize)) {
//            page = 1;
//        }
//        List<Post> posts = bs.getPostsByCategories(filterPost, filter, editor.getUserid(), categoriesFilter, pageSize, page, sortBy, orderBy, keyword);
//        /**
//         * End Paging
//         */
        return SUCCESS;
    }

    public String[] getAction() {
        return action;
    }

    public void setAction(String[] action) {
        this.action = action;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Long getCategoryChoose() {
        return categoryChoose;
    }

    public void setCategoryChoose(Long categoryChoose) {
        this.categoryChoose = categoryChoose;
    }

    public Long getCountAll() {
        return countAll;
    }

    public void setCountAll(Long countAll) {
        this.countAll = countAll;
    }

    public Long getCountDraft() {
        return countDraft;
    }

    public void setCountDraft(Long countDraft) {
        this.countDraft = countDraft;
    }

    public Long getCountPending() {
        return countPending;
    }

    public void setCountPending(Long countPending) {
        this.countPending = countPending;
    }

    public Long getCountPublished() {
        return countPublished;
    }

    public void setCountPublished(Long countPublished) {
        this.countPublished = countPublished;
    }

    public Long getCountTrash() {
        return countTrash;
    }

    public void setCountTrash(Long countTrash) {
        this.countTrash = countTrash;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<KeyValue> getListOrderBy() {
        return listOrderBy;
    }

    public void setListOrderBy(List<KeyValue> listOrderBy) {
        this.listOrderBy = listOrderBy;
    }

    public List<KeyValue> getListSortBy() {
        return listSortBy;
    }

    public void setListSortBy(List<KeyValue> listSortBy) {
        this.listSortBy = listSortBy;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public Long[] getPostids() {
        return postids;
    }

    public void setPostids(Long[] postids) {
        this.postids = postids;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getFilterPost() {
        return filterPost;
    }

    public void setFilterPost(String filterPost) {
        this.filterPost = filterPost;
    }

    public List<String> getListFilterPost() {
        return listFilterPost;
    }

    public void setListFilterPost(List<String> listFilterPost) {
        this.listFilterPost = listFilterPost;
    }

   
}
