/*
 * Copyright 2016 Certainty Health
 */
package com.vietdiep.enews.action;

import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.bussiness.CategoryBusiness;
import com.vietdiep.enews.bussiness.PostBusiness;
import com.vietdiep.enews.model.Advertisement;
import java.util.List;
import com.vietdiep.enews.model.Category;
import com.vietdiep.enews.model.Post;
import com.vietdiep.enews.model.PostViewer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
/**
 *
 * @author VietDiep
 */
public class HomeAction extends ActionSupport {

    // Services
    //CategoryService categoryService = new CategoryServiceImpl();
    //PostService postService = new PostServiceImpl();
    //AdsService adsService = new AdsServiceImpl();
    private CategoryBusiness categoryService = CategoryBusiness.getInstance();
    private PostBusiness postService = PostBusiness.getIntance();
    // Models
    private List<Category> categories;
    private List<Post> hotPosts;
    Post hotPostTop1 = null;    
    List<Post> hotPostTop234 = null;
    List<Post> lastedPublishedPosts;
    List<Post> highViewPosts = null;
    List<Advertisement> advertisements;
    List<Post> postPublishedByCategory = null;
    
    PostViewer hotPostTop1View = null;    
    List<PostViewer> hotPostTop234View = null;
    List<PostViewer> lastedPublishedPostsView;
    List<PostViewer> highViewPostsView = null;
    List<Advertisement> advertisementsView;
    List<PostViewer> postPublishedByCategoryView = null;
    
    
    @Override
    public String execute() {
        try {
            categories = categoryService.getCategories();
            hotPostTop1 = postService.getHotPostTop1();
            hotPostTop1View = PostViewer.instance.getPostViewer(hotPostTop1);
            hotPostTop234 = postService.getHotPostTop234();
            hotPostTop234View = PostViewer.instance.getListPostViewer(hotPostTop234);
            lastedPublishedPosts = postService.getLastedPublishedHightlightPosts();
            lastedPublishedPostsView = PostViewer.instance.getListPostViewer(lastedPublishedPosts);
            highViewPosts = postService.getHighViewsPublishedPost();
            highViewPostsView = PostViewer.instance.getListPostViewer(highViewPosts);
        } catch (Exception ex) {
            Logger.getLogger(HomeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return SUCCESS;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Post> getHotPosts() {
        return hotPosts;
    }

    public void setHotPosts(List<Post> hotPosts) {
        this.hotPosts = hotPosts;
    }

    public Post getHotPostTop1() {
        return hotPostTop1;
    }

    public void setHotPostTop1(Post hotPostTop1) {
        this.hotPostTop1 = hotPostTop1;
    }

    public List<Post> getHotPostTop234() {
        return hotPostTop234;
    }

    public void setHotPostTop234(List<Post> hotPostTop234) {
        this.hotPostTop234 = hotPostTop234;
    }

    public List<Post> getLastedPublishedPosts() {
        return lastedPublishedPosts;
    }

    public void setLastedPublishedPosts(List<Post> lastedPublishedPosts) {
        this.lastedPublishedPosts = lastedPublishedPosts;
    }

    public List<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(List<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }

    public CategoryBusiness getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(CategoryBusiness categoryService) {
        this.categoryService = categoryService;
    }

    public PostBusiness getPostService() {
        return postService;
    }

    public void setPostService(PostBusiness postService) {
        this.postService = postService;
    }

    public List<Post> getHighViewPosts() {
        return highViewPosts;
    }

    public void setHighViewPosts(List<Post> highViewPosts) {
        this.highViewPosts = highViewPosts;
    }

    public List<Post> getPostPublishedByCategory() {
        return postPublishedByCategory;
    }

    public void setPostPublishedByCategory(List<Post> postPublishedByCategory) {
        this.postPublishedByCategory = postPublishedByCategory;
    }

    

    public List<Advertisement> getAdvertisementsView() {
        return advertisementsView;
    }

    public void setAdvertisementsView(List<Advertisement> advertisementsView) {
        this.advertisementsView = advertisementsView;
    }

    public PostViewer getHotPostTop1View() {
        return hotPostTop1View;
    }

    public void setHotPostTop1View(PostViewer hotPostTop1View) {
        this.hotPostTop1View = hotPostTop1View;
    }

    public List<PostViewer> getHotPostTop234View() {
        return hotPostTop234View;
    }

    public void setHotPostTop234View(List<PostViewer> hotPostTop234View) {
        this.hotPostTop234View = hotPostTop234View;
    }

    public List<PostViewer> getLastedPublishedPostsView() {
        return lastedPublishedPostsView;
    }

    public void setLastedPublishedPostsView(List<PostViewer> lastedPublishedPostsView) {
        this.lastedPublishedPostsView = lastedPublishedPostsView;
    }

    public List<PostViewer> getHighViewPostsView() {
        return highViewPostsView;
    }

    public void setHighViewPostsView(List<PostViewer> highViewPostsView) {
        this.highViewPostsView = highViewPostsView;
    }

    public List<PostViewer> getPostPublishedByCategoryView() {
        return postPublishedByCategoryView;
    }

    public void setPostPublishedByCategoryView(List<PostViewer> postPublishedByCategoryView) {
        this.postPublishedByCategoryView = postPublishedByCategoryView;
    }
    
}
