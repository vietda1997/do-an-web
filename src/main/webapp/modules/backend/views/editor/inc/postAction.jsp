<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<h2>Posts</h2>

<div class="tab1">
    <ul>
        <li>
            <a href="?t=all">
                <s:if test="filter == 'all'">
                    <span class="selected">All</span>
                </s:if>
                <s:else>
                    All
                </s:else>
                <span class="datetime">(${countAll})</span>
            </a>
        </li>
        <li>
            <a href="?t=pending">
                <s:if test="filter == 'pending'">
                    <span class="selected">Pending</span>
                </s:if>
                <s:else>
                    Pending
                </s:else>
                <span class="datetime">(${countPending})</span>
            </a>
        </li>
        <li>
            <a href="?t=published">
                <s:if test="filter == 'published'">
                    <span class="selected">Published</span>
                </s:if>
                <s:else>
                    Published
                </s:else>
                <span class="datetime">(${countPublished})</span>
            </a>
        </li>
        <li>
            <a href="?t=draft">
                <s:if test="filter == 'draft'">
                    <span class="selected">Draft</span>
                </s:if>
                <s:else>
                    Draft
                </s:else>
                <span class="datetime">(${countDraft})</span>
            </a>
        </li>
        <li>
            <a href="?t=trash">
                <s:if test="filter == 'trash'">
                    <span class="selected">Trash</span>
                </s:if>
                <s:else>
                    Trash
                </s:else>
                <span class="datetime">(${countTrash})</span>
            </a>
        </li>
    </ul>
</div>

<div class="clear"></div><br />
<s:if test="actionMessages.size() > 0">
    <div class="success">
        <s:property value="actionMessages.get(0)" />
    </div>
</s:if>
<form action="?t=${filter}" method="post" name="posts">
    <div class="leftmanagefunction">
        <select onchange="doAction(document.posts, this, new Array(1,2), document.posts.postids);"  name="action" size="1" id="selectpermission">
            <option selected="selected">Actions</option>
            <s:if test="filter == 'trash'">
                <option value="restore">Restore</option>
                <option value="deletepermanently">Delete Permanently</option>
            </s:if>
            <s:else>
                <option value="trash">Move to trash</option>
            </s:else>
        </select>
    </div>
    <div class="rightmanagefunction">
        Keyword:
        <s:textfield name="keyword" cssStyle="width: 100px"/>
        |
        Sort:
        <s:select list="listSortBy" listKey="key" listValue="value" name="sortBy"/>
        <s:select list="listOrderBy" listKey="key" listValue="value" name="orderBy"/>
        | Filter:
        <s:select list="listFilterPost" name="filterPost"/>
        <s:select list="categories" listKey="categoryid" listValue="categoryName" name="categoryChoose"/>
        <input class="buttonfunction" type="submit" value="Filter" />
    </div>
    <div class="clear"></div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table managetable-row-showaction">
        <tr>
            <td class="head checkselect" width="10px"><input onchange="toggleCheckAll(document.posts.postids, document.posts.checkAll, this.checked);" name="checkAll" type="checkbox" value="" /></td>
            <td class="head">Title</td>
            <td class="head" width="100px">Reporter</td>
            <td class="head" width="60px">View</td>
            <td class="head" width="90px">Categories</td>
            <td class="head" width="120px">Date</td>
        </tr>

        <s:iterator value="objPaging.objArray">
            <tr>
                <td class="checkselect">
                    <s:checkbox onchange="unCheckCheckAll(document.posts.checkAll, this)" theme="simple" name="postids" fieldValue="%{postid}"/>
                </td>
                <td class="maintext">
                    ${maxVersionPost.title}
                    <s:if test="maxVersionPost.version > 1">
                        <span class="versionsign">[${maxVersionPost.version}]</span>
                    </s:if>
                    <s:if test="publishedpost != null && publishedpost.isHighlights == true">
                        <span class="hotnewssign"><abbr title="Highlights">[HL]</abbr></span>
                    </s:if>
                    <s:if test="publishedpost != null && publishedpost.isHot == true">
                        <span class="hotnewssign"><abbr title="HOT">[Hot]</abbr></span>
                    </s:if>
                    <br />
                    <div class="doactionacticles">
                        <s:url action="news" namespace="/web" var="linkView">
                            <s:param name="pid">${postid}</s:param>
                        </s:url>
                        <s:url action="addpost" namespace="/backend" var="linkEdit">
                            <s:param name="vid">${maxVersionPost.versionpostid}</s:param>
                        </s:url>
                        <s:url action="poststats" namespace="/backend" var="linkStats">
                            <s:param name="pid">${postid}</s:param>
                        </s:url>
                        <s:if test="isPublished == true"><a href="${linkView}" target="_blank">View</a> | <a href="${linkStats}" target="_blank">Stats</a></s:if>
                        <s:if test="isTrash == false">
                             | <a href="${linkEdit}">Edit</a>
                        </s:if>
                    </div>
                </td>
                <td>
                    <s:url action="viewreporter" namespace="/backend" var="linkView">
                        <s:param name="uname">${author.username}</s:param>
                    </s:url>
                    <a href="${linkView}" target="_blank">${author.username}</a>
                </td>
                <td>
                    ${totalPageView}
                </td>
                <td>
                    ${category.categoryName}
                </td>
                <td>
                    ${formatlastmodified}
                    <br/>
                    <s:if test="isTrash">
                        Last Modified
                    </s:if>
                    <s:elseif test="isDraft">
                        Draft
                    </s:elseif>
                    <s:elseif test="isPublished">
                        Published
                    </s:elseif>
                    <s:else>
                        Pending
                    </s:else>
                </td>
            </tr>
        </s:iterator>
        <s:if test="objPaging.countDisplay == 1">
            <s:hidden theme="simple" name="postids"/>
        </s:if>
        <tr>
            <td class="head checkselect"><input onchange="toggleCheckAll(document.posts.postids, document.posts.checkAll, this.checked);" name="checkAll" type="checkbox" value="" /></td>
            <td class="head">Title</td>
            <td class="head">Reporter</td>
            <td class="head">View</td>
            <td class="head">Categories</td>
            <td class="head">Date</td>
        </tr>
    </table>
    <div class="leftmanagefunction">
        <select onchange="doAction(document.posts, this, new Array(1,2), document.posts.postids);"  name="action" size="1" id="selectpermission">
            <option selected="selected">Actions</option>
            <s:if test="filter == 'trash'">
                <option value="restore">Restore</option>
                <option value="deletepermanently">Delete Permanently</option>
            </s:if>
            <s:else>
                <option value="trash">Move to trash</option>
            </s:else>
        </select>
    </div>
    <div class="rightmanagefunction">
        ${objPaging.startIndex} - ${objPaging.endIndex} / ${objPaging.totalRecord} Posts
        <s:if test="objPaging.prevPage > 0">
            <input onclick="document.posts.action = '?t=${filter}&p=${objPaging.prevPage}'" class="buttonfunction" type="submit" value="Previous" name="buttonClick"/>
        </s:if>
        <s:if test="objPaging.nextPage > 0">
            <input onclick="document.posts.action = '?t=${filter}&p=${objPaging.nextPage}'" class="buttonfunction" type="submit" value="Next"  name="buttonClick"/>
        </s:if>
    </div>
</form>