/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.bussiness;

import com.vietdiep.enews.database.DBConnection;
import com.vietdiep.enews.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vietda
 */
public class UserBusiness {
    
    public static UserBusiness instance = new UserBusiness();
    
    public List<User> getListUser() {
        List<User> users = null;
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            String string = "select * from USERS";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                ResultSet rs = statement.executeQuery();
                users = new ArrayList<>();
                while (rs.next()) {
                    User user = new User();
                    user.setCreateDTM(rs.getLong("createdDtm"));
                    user.setEmail(rs.getString("email"));
                    user.setFullName(rs.getString("fullName"));
                    user.setId(rs.getInt("userID"));
                    user.setPassWord(rs.getString("passWord"));
                    user.setRole(rs.getString("role"));
                    user.setUserName(rs.getString("userName"));
                    user.setIsLock(rs.getInt("isLock"));
                    user.setPhoneNumber(rs.getString("phoneNumber"));
                    user.setAddress(rs.getString("address"));
                    user.setLastUpdateTime(rs.getLong("lastUpdateTime"));
                    users.add(user);
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserBusiness.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }

        }
        return users;
    }
    
    public User getUserByID(int id) {
        Connection connection = DBConnection.getInstance().getConnection();
        User user = null;
        if (connection != null) {
            try {
                String string = "select * from USERS where userID = ?";
                PreparedStatement statement = connection.prepareStatement(string);
                statement.setInt(1, id);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    user = new User();
                    user.setCreateDTM(rs.getLong("createdDtm"));
                    user.setEmail(rs.getString("email"));
                    user.setFullName(rs.getString("fullName"));
                    user.setId(rs.getInt("userID"));
                    user.setPassWord(rs.getString("passWord"));
                    user.setRole(rs.getString("role"));
                    user.setUserName(rs.getString("userName"));
                    user.setIsLock(rs.getInt("isLock"));
                    user.setPhoneNumber(rs.getString("phoneNumber"));
                    user.setAddress(rs.getString("address"));
                    user.setLastUpdateTime(rs.getLong("lastUpdateTime"));
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserBusiness.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }

        }
        return user;
    }
    
    public int updateUser(User user){
        Connection connection = DBConnection.getInstance().getConnection();
        if(connection!=null){
            String string = "update USERS set email = ?,"
                    + " fullName = ?, "
                    + "passWord = ?, "
                    + "role = ?, "
                    + "userName = ?, "
                    + "isLock = ?, "
                    + "phoneNumber = ?, "
                    + "address = ?, "
                    + "lastUpdateTime = ? where userID = ?";
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(string);
                preparedStatement.setString(1,user.getEmail());
                preparedStatement.setString(2,user.getFullName());
                preparedStatement.setString(3,user.getPassWord());
                preparedStatement.setString(4,user.getRole());
                preparedStatement.setString(5,user.getUserName());
                preparedStatement.setInt(6,user.isIsLock());
                preparedStatement.setString(7,user.getPhoneNumber());
                preparedStatement.setString(8,user.getPhoneNumber());
                preparedStatement.setLong(9, user.getLastUpdateTime());
                preparedStatement.setInt(10, user.getId());
                int resultSet = preparedStatement.executeUpdate();
                return resultSet;
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return 0;
    }
    
    public int insertUser(User user){
        Connection connection = DBConnection.getInstance().getConnection();
        if(connection == null){
            return 0;
        }
        String string = "insert into USERS(createdDtm,email,fullName,passWord,userName,role,isLock,phoneNumber,address,lastUpdateTime)"
                + "values (?,?,?,?,?,?,?,?,?,?)";
        try {
                PreparedStatement preparedStatement = connection.prepareStatement(string);
                preparedStatement.setLong(1,user.getCreateDTM());
                preparedStatement.setString(2,user.getEmail());
                preparedStatement.setString(3,user.getFullName());
                preparedStatement.setString(4,user.getPassWord());
                preparedStatement.setString(5,user.getUserName());
                preparedStatement.setString(6,user.getRole());
                preparedStatement.setInt(7,user.isIsLock());
                preparedStatement.setString(8,user.getPhoneNumber());
                preparedStatement.setString(9, user.getAddress());
                preparedStatement.setLong(10, user.getLastUpdateTime());
                int resultSet = preparedStatement.executeUpdate();
                return resultSet;
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        return 0;
    }
    // get all user by role
    public List<User> getListUserByRole(String role){
        List<User> users = null;
        Connection connection = DBConnection.getInstance().getConnection();
        if (connection != null) {
            String string = "select * from USERS WHERE role = ?";
            try {
                PreparedStatement statement = connection.prepareStatement(string);
                statement.setString(1, role);
                ResultSet rs = statement.executeQuery();
                users = new ArrayList<>();
                while (rs.next()) {
                    User user = new User();
                    user.setCreateDTM(rs.getLong("createdDtm"));
                    user.setEmail(rs.getString("email"));
                    user.setFullName(rs.getString("fullName"));
                    user.setId(rs.getInt("userID"));
                    user.setPassWord(rs.getString("passWord"));
                    user.setRole(rs.getString("role"));
                    user.setUserName(rs.getString("userName"));
                    user.setIsLock(rs.getInt("isLock"));
                    user.setPhoneNumber(rs.getString("phoneNumber"));
                    user.setAddress(rs.getString("address"));
                    user.setLastUpdateTime(rs.getLong("lastUpdateTime"));
                    users.add(user);
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserBusiness.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                DBConnection.getInstance().returnConnectionToPool(connection);
            }
        }
        return users;
    }
    
    
}
