<%-- 
    Document   : left
    Created on : Oct 18, 2017, 3:10:15 PM
    Author     : Viet Diep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<div id="hotnews">
    <jsp:include page="hotnews.jsp" />
</div>

<div id="folder">
    <s:iterator value="categories">
       
        <s:url var="categoryLink" action="category" >
            <s:param name="categoryid" value="categoryID"/>
        </s:url>
        
        <div class="folder-title">
            <div class="folder-title-sub-selected">
                <a href="${categoryLink}"><strong>${categoryName}</strong></a>
            </div>
            <s:iterator value="childCategories">
                <s:url var="categoryLink" action="category" >
                    <s:param name="categoryid" value="categoryID"/>
                </s:url>
                <div class="folder-title-sub">
                    <a href="${categoryLink}"> ${categoryName}</a>
                </div>
            </s:iterator>
        </div>
        <div class="clear"></div>
        
        <s:action name="publishedByCategory" ignoreContextParams="true" executeResult="true">
            <s:param name="cid" value="categoryID"/>
        </s:action>
        
    </s:iterator>
</div>
