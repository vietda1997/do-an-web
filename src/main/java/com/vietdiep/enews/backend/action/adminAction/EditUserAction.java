/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vietdiep.enews.backend.action.adminAction;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.bussiness.UserBusiness;
import com.vietdiep.enews.model.User;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vietda
 */
public class EditUserAction extends ActionSupport {

    String emailAgain;
    List<String> listPermission = Arrays.asList(
            new String[]{"", "Admin", "Editor", "Reporter", "Comment Approver",});
    User user;
    String permissionChoose;
    String requestMeta = "";
    String userName = "";
    String password = "";
    String passwordAgain;
    String email = "";
    String fullName = "";
    String address = "";
    String phoneNumber = "";
    int isLock = 0;
    static boolean isFirstVisit = true;
    static int userID;

    @Override
    public String execute() {
        this.requestMeta = "editUser";

        if (isFirstVisit) {
            Map m = ActionContext.getContext().getParameters();
            String value = m.get("userid").toString();
            int userId = Integer.parseInt(value);
            userID = userId;
            User user = UserBusiness.instance.getUserByID(userId);
            this.emailAgain = user.getEmail();
            this.userName = user.getUserName();
            this.email = user.getEmail();
            this.fullName = user.getFullName();
            this.address = user.getAddress();
            this.phoneNumber = user.getPhoneNumber();
            this.isLock = user.isIsLock();
            this.isFirstVisit = false;
        } else {
            user = new User();
            user.setLastUpdateTime(System.currentTimeMillis());
            user.setRole(permissionChoose);
            user.setUserName(this.userName);
            user.setPassWord(this.password);
            user.setEmail(this.email);
            user.setFullName(this.fullName);
            user.setAddress(this.address);
            user.setPhoneNumber(this.phoneNumber);
            user.setIsLock(this.isLock);
            user.setId(userID);
            UserBusiness.instance.updateUser(user);
        }
        return INPUT;
    }

    @Override
    public void validate() {
        this.requestMeta = "editUser";
        if (isFirstVisit) {
            return;
        }
        if (this.userName.equals("")) {
            addFieldError("invUsername", "Username is required");
        }
        if (this.password.equalsIgnoreCase("")) {
            addFieldError("invPassword", "Password is requied");
        }
        if (!this.passwordAgain.equalsIgnoreCase(this.password)) {
            addFieldError("invPassword", "Password is wrong");
        }
        if (this.email.equalsIgnoreCase("")) {
            addFieldError("invEmail", "Email is required");
        }
        if (!this.emailAgain.equalsIgnoreCase(this.emailAgain)) {
            addFieldError("invEmail", "Email Again is wrong");
        }
        if (this.permissionChoose.equals("")) {
            addFieldError("invPermission", "Permission is required");
        }
        if (this.fullName.equalsIgnoreCase("")) {
            addFieldError("invFullname", "Fullname is required");
        }
        if (this.address.equalsIgnoreCase("")) {
            addFieldError("invAddress", "Address is required");
        }
        if (this.phoneNumber.equalsIgnoreCase("")) {
            addFieldError("invPhonenumber", "Phone Number is required");
        }
    }

    public String getPasswordAgain() {
        return passwordAgain;
    }

    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }

    public String getEmailAgain() {
        return emailAgain;
    }

    public void setEmailAgain(String emailAgain) {
        this.emailAgain = emailAgain;
    }

    public List<String> getListPermission() {
        return listPermission;
    }

    public void setListPermission(List<String> listPermission) {
        this.listPermission = listPermission;
    }

    public String getRequestMeta() {
        return requestMeta;
    }

    public void setRequestMeta(String requestMeta) {
        this.requestMeta = requestMeta;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPermissionChoose() {
        return permissionChoose;
    }

    public void setPermissionChoose(String permissionChoose) {
        this.permissionChoose = permissionChoose;
    }

    public boolean isIsFirstVisit() {
        return isFirstVisit;
    }

    public void setIsFirstVisit(boolean isFirstVisit) {
        this.isFirstVisit = isFirstVisit;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getIsLock() {
        return isLock;
    }

    public void setIsLock(int isLock) {
        this.isLock = isLock;
    }

}
