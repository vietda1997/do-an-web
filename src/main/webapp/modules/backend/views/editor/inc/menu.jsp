<%-- 
    Document   : menu.jsp
    Created on : Jan 9, 2019, 10:41:47 PM
    Author     : vietda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@taglib prefix="s" uri="/struts-tags"%>
<s:url action="dashboarde" namespace="/backend" var="linkDashboard"></s:url>
<a href="${linkDashboard}">
    <div class="boxtype1-title">
        Dashboard
    </div>
</a>

<br/>

<div class="boxtype1-title">
    Posts
</div>
<div class="boxtype1-content">
    <ul class="listcategory">
        <li>
            <s:url action="postse" namespace="/backend" var="linkPosts"/>
            <a href="${linkPosts}">+ Posts</a>
        </li>
    </ul>
</div>

<br />

<div class="boxtype1-title">
    Statistic
</div>
<div class="boxtype1-content">
    <ul class="listcategory">
        <li>
            <s:url action="topcategories" namespace="/backend" var="linkTopCategories"/>
            <a href="${linkTopCategories}">+ Top Categories</a>
        </li>
        <li>
            <s:url action="toppostse" namespace="/backend" var="linkTopPosts"/>
            <a href="${linkTopPosts}">+ Top Posts</a>
        </li>
        <li>
            <s:url action="topreporters" namespace="/backend" var="linkTopReporters"/>
            <a href="${linkTopReporters}">+ Top Reporters</a>
        </li>
        <li>
            <s:url action="totalviewstats" namespace="/backend" var="linkTotalView"/>
            <a href="${linkTotalView}">+ Total View Stats</a>
        </li>
        <li>
            <s:url action="averageviewstats" namespace="/backend" var="linkAverageViewStats"/>
            <a href="${linkAverageViewStats}">+ Average View Stats</a>
        </li>
        <li>
            <s:url action="viewreporter" namespace="/backend" var="linkViewReporter"/>
            <a href="${linkViewReporter}">+ View Reporter</a>
        </li>
    </ul>
</div>

<br />

<s:url action="profile" namespace="/backend" var="linkProfile"></s:url>
<a href="${linkProfile}">
    <div class="boxtype1-title">
        Profile
    </div>
</a>