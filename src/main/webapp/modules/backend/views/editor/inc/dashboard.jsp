<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<h2>Dashboard</h2>

<div class="dashboard-left">
    <div class="boxborder">
        <h4 class="blocksubhead">Editor</h4>

        <ul class="listinfor">
            <li>
                <label>Categories: </label>
                <s:iterator value="categories" status="row">
                    <s:url action="category" namespace="/web" var="linkView">
                        <s:param name="cid">${categoryID}</s:param>
                    </s:url>
                    <a href="${linkView}" target="_blank">${categoryName}</a><s:if test="!#row.last">,</s:if>
                </s:iterator>
            </li>
            <li>
                <label>Reporters:</label>
                <s:iterator value="reporters" status="row">
                    <s:url action="profile" namespace="/backend" var="linkView">
                        <s:param name="uid">${id}</s:param>
                    </s:url>
                    <a href="${linkView}">${userName}</a><s:if test="!#row.last">,</s:if>
                </s:iterator>
            </li>
            <li><label>Statistic:</label><br />
                <ul class="listinfor">
                    <li>
                        <label><strong>${countAll}</strong></label> Posts
                    </li>
                    <li style="color: red">
                        <label><strong>${countPending}</strong></label> Pending
                    </li>
                    <li>
                        <label><strong>${countPublished}</strong></label> Published
                    </li>
                    <li>
                        <label><strong>${countDraft}</strong></label> Drafts
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <br />
    <div class="boxborder">
        <h4 class="blocksubhead">Recent Published</h4>

        <ul class="listcategory">
            <s:iterator value="recentPublished">
            <li>
                <s:url action="news" namespace="/web" var="linkView">
                    <s:param name="postid">${postID}</s:param>
                </s:url>
                <p class="maintext"><a href="${linkView}" target="_blank">${title}</a></p>
                <p><span class="datetime">${createdDTM}</span></p>
                <p>${shortDescription}</p>
            </li>
            </s:iterator>
            <li style="text-align: right">
                <s:url action="postse" namespace="/backend" var="linkViewAll">
                    <s:param name="t">published</s:param>
                </s:url>
                <a href="${linkViewAll}">View All</a>
            </li>
        </ul>
    </div>
</div>
<div class="dashboard-right">
    <div class="boxborder">
        <h4 class="blocksubhead">Recent Pending</h4>

        <ul class="listcategory">
            <s:iterator value="recentPending">
            <li>
                <s:url action="addpost" namespace="/backend" var="linkView">
                    <s:param name="vid">${postID}</s:param>
                </s:url>
                <p class="maintext"><a href="${linkView}">${maxVersionPost.title}</a></p>
                <p><span class="datetime">${createdDTM}</span></p>
                <p>${shortDescription}</p>
            </li>
            </s:iterator>
            <li style="text-align: right">
                <s:url action="postse" namespace="/backend" var="linkViewAll">
                    <s:param name="t">pending</s:param>
                </s:url>
                <a href="${linkViewAll}">View All</a>
            </li>
        </ul>
    </div>
    <br />
    <div class="boxborder">
        <h4 class="blocksubhead">Recent Drafts</h4>
        <ul class="listcategory">
            <s:iterator value="recentDraft">
            <li>
                <s:url action="addpost" namespace="/backend" var="linkView">
                    <s:param name="vid">${postID}</s:param>
                </s:url>
                <p class="maintext"><a href="${linkView}">${title}</a></p>
                <p><span class="datetime">${createdDTM}</span></p>
                <p>${shortDescription}</p>
            </li>
            </s:iterator>
            <li style="text-align: right">
                <s:url action="postse" namespace="/backend" var="linkViewAll">
                    <s:param name="t">draft</s:param>
                </s:url>
                <a href="${linkViewAll}">View All</a>
            </li>
        </ul>
    </div>
</div>