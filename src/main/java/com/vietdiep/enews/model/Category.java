/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.model;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Viet Diep
 */

public class Category {
    
    int categoryID;
    int parentID;
    String categoryName;
    int positionNumber;
    long CreatedDtm;
    int totalView;
    List<Category> childCategories; 

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getParentID() {
        return parentID;
    }

    public void setParentID(int parentID) {
        this.parentID = parentID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    
    public int getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(int positionNumber) {
        this.positionNumber = positionNumber;
    }

    public long getCreatedDtm() {
        return CreatedDtm;
    }

    public void setCreatedDtm(long CreatedDtm) {
        this.CreatedDtm = CreatedDtm;
    }

    public int getTotalView() {
        return totalView;
    }

    public void setTotalView(int totalView) {
        this.totalView = totalView;
    }

    public List<Category> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(List<Category> childCategories) {
        this.childCategories = childCategories;
    }
    
}
