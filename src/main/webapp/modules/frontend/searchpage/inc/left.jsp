<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>

<sx:head/>
<form action="search" method="post" name="search">
    <h4 class="blocksubhead">Tìm kiếm</h4>
    <div class="search-condition">
        <p><b>Từ khóa hoặc cụm từ cần tìm:</b></p>
        <p><s:textfield theme="simple" cssStyle="width: 400px" name="keyWord"/>
            <input type="submit" value="Tìm kiếm"/>
        <p><b>Thông tin nâng cao:</b></p>
        <table style="margin-left: 10px" width="500px" cellpadding="5" cellspacing="2" border="0">
            <tr>
                <td width="200px">Từ</td>
            <td><sx:datetimepicker name="fromDate" displayFormat="dd/MM/yyyy"/></td>
            </tr>
            <tr>
                <td>Đến</td>
                <td>
                    <sx:datetimepicker name="toDate" displayFormat="dd/MM/yyyy"/>
                    <span class="errorMessage"><s:property value="fieldErrors.get('invTodate').get(0)" /></span>
                </td>
            </tr>
            <tr>
                <td>Ngày</td>
                <td>
                    <s:select theme="simple" list="listOrderBy" listKey="key" listValue="value" name="orderBy"/>
                </td>
            </tr>
            <tr>
                <td>Chuyên mục</td>
                <td>
                    <s:select theme="simple" list="listCategoryKeyValue" listKey="key" listValue="value" name="categoryChoose" />
                </td>
            </tr>
        </table>
    </div>


    <h4 class="blocksubhead">Kết quả tìm kiếm</h4>
    <s:if test="postViewer.size() > 0">
        <div class="clear"></div>
        <s:iterator value="postViewer" status="status">
            <s:url action="news" var="linkView">
                <s:param name="postid" value="postID"/>
            </s:url>
            <div class="folder-news">
                <p><a href="${linkView}">
                        <s:if test="avatarImage != ''">
                            <img class="folder-news-image" src="${avatarImage}" onerror="this.onerror=null; this.style.display='none';"/>
                        </s:if>
                    </a></p>
                <p>
                    <a href="${linkView}" class="folder-topnews-title">${title}</a>
                </p>
                <p class="datetime">
                    ${updatedDTM}
                </p>
                
                <span class="relationposts">
                    <s:iterator value="postViewer">
                        <s:url action="news" var="linkView">
                            <s:param name="postid" value="postID"/>
                        </s:url>
                        <p>&gt; <a href="${linkView}">${title}</a></p>
                    </s:iterator>
                </span>
            </div>
        </s:iterator>
    </s:if>
    <s:else>
        <div style="padding: 8px;" align="center">
            Không tìm thấy kết quả
        </div>
    </s:else>

</form>
