<%-- 
    Document   : left
    Created on : Oct 28, 2017, 10:36:58 AM
    Author     : Viet Diep
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<h4 class="blocksubhead">
    <s:if test="category.parentCategoryName != null">
        ${category.parentCategoryName} >
    </s:if>
    ${category.categoryName}
</h4>

<div id="news-detail-content">
    <p class="news-detail-title">
        ${postView.title}
    </p>
    
    <p><span class="datetime">${postView.createdDTM}</span></p>
    <s:if test="postView.avatarImage != ''">
        <img src="${postView.avatarImage}" id="news-detail-image" width="250" height="195" onerror="this.onerror=null; this.style.display='none';"/>
    </s:if>
    <p style="font-size: 13px"><b>${postView.shortDescription}</b></p>
    ${postView.content}
    <br class="clear"/>
</div> 

<a name="comments"></a>
<h4 class="blocksubhead">Bình luận</h4>

<s:iterator value="comments">
    <a name="cmid${id}"></a>
    <div class="blockrow-content">
        <h4>${authorName}<span class="dateright">${createdTime}</span></h4>
        <p>${content}</p>
        
    </div>
</s:iterator>

<a name="postcomment"></a>
<a href="javascript:" onclick="$('#frompostcomment').slideToggle('fast');"><h4 class="blocksubhead" style="color: #000">Gửi bình luận</h4></a>
<s:form action="postComment" method="post" onsubmit="return checkPostComment();">
    &nbsp;
    <div id="frompostcomment" <s:if test="comment == null"> style="display: none"</s:if>>
        <s:if test="actionErrors.size() > 0">
            <div class="blockrow-first">
                <span class="errorMessage">
                    <s:property value="actionErrors.get(0)" />
                </span>
            </div>
        </s:if>
        
        <s:if test="actionMessages.size() > 0">
            <div class="blockrow-first">
                <span class="successMessage">
                    <s:property value="actionMessages.get(0)" />
                </span>
            </div>
        </s:if>
        
        <div class="blockrow-first">
            <p><label>Tên người gửi (<font color="red">*</font>):</label>
                <s:textfield theme="simple" name="comment.authorName"/></p>
        </div>
        <div class="blockrow-first">
            <p><label>Nội dung (<font color="red">*</font>):</label>
                <s:textarea theme="simple" name="comment.content" cssStyle="width:100%; margin-top: 8px" rows="5"></s:textarea>
            </p>
        </div>
        <div class="blockrow-first">
            <p>
                <label>Code (<font color="red">*</font>):</label>
                <span align="center"><s:textfield theme="simple" name="code" /></span>
            </p>
        </div>
            <label>&nbsp;</label>
        <div align="center">
            <span class="seccode"><s:property value="123456"/></span>
            <div style="opacity:0; height: 100%;position: absolute;top: 0px;width: 140px; background-color: #0000B9"></div>
        </div>
        <div class="blockrow-content">
            <div class="blockrow-right">
                <input type="submit" value="Gửi"/>
                <input type="reset" value="Làm lại"/>
            </div>
        </div>
    </div>
</s:form>


<h4 class="blocksubhead">Tin khác</h4>
<div class="foldernews-older-list">
    <div class="newnews">
        <s:iterator value="otherPostsView">
            <s:url action="news" var="linkView">
                <s:param name="postid" value="postID"/>
            </s:url>
            <p>+ <a href="${linkView}">${title}</a> <span class="datetime">- ${createdDTM}</span></p>
        </s:iterator>
    </div>
</div>

