<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<div id="content">
    <div id="left">
            <jsp:include page="inc/menu.jsp"/>
    </div>
    <div id="right">
            <jsp:include page="inc/${requestMeta}.jsp"/>
    </div>
</div>