/*
 * Copyright 2017 Home Ltd.
 */
package com.vietdiep.enews.action;

import com.opensymphony.xwork2.ActionSupport;
import com.vietdiep.enews.bussiness.CategoryBusiness;
import com.vietdiep.enews.bussiness.PostBusiness;
import com.vietdiep.enews.model.Category;
import com.vietdiep.enews.model.KeyValue;
import com.vietdiep.enews.model.Post;
import com.vietdiep.enews.model.PostViewer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Viet Diep
 */
public class SearchPageAction extends ActionSupport {

    CategoryBusiness categoryService = new CategoryBusiness();
    PostBusiness postService = new PostBusiness();
    List<Category> categories;
    List<Post> posts;
    List<PostViewer> postViewer;
    String oderBy;
    List<KeyValue> listOrderBy = Arrays.asList(
            new KeyValue[]{new KeyValue("desc", "Giảm dần"), new KeyValue("asc", "Tăng dần")});
    Date fromDate;
    Date toDate;
    String keyWord = "";
    String orderBy;
    String categoryChoose = "";
    static boolean isFirst = true;
    List<KeyValue> listCategoryKeyValue = Arrays.asList( new KeyValue[]{new KeyValue("0", "Tất cả chuyên mục"), new KeyValue("1", "Xã Hội"), new KeyValue("11", "Thời Sự"), 
    new KeyValue("12", "Giao Thông"), new KeyValue("13", "Môi Trường - Khí Hậu"), new KeyValue("2", "Thế Giới"), new KeyValue("21", "Văn Hóa"),
    new KeyValue("22", "Nghệ Thuật"), new KeyValue("23", "Ẩm Thực"), new KeyValue("24", "Du Lịch"), new KeyValue("3", "Kinh Tế"),
    new KeyValue("31", "Kinh Doanh"), new KeyValue("32", "Lao Động"), new KeyValue("33", "Tài Chính"), new KeyValue("34", "Chứng Khoán"),
    new KeyValue("4", "Thể Thao"), new KeyValue("41", "Bóng Đá Quốc Tế"), new KeyValue("42", "Bóng Đá Việt Nam"), new KeyValue("43", "Quần Vợt"),
    new KeyValue("51", "Âm Nhạc"), new KeyValue("52", "Thời Trang"), new KeyValue("53", "Điện Ảnh - Truyền Hình")});

    @Override
    public String execute() throws SQLException {
        if (isFirst) {
            isFirst = false;
            return SUCCESS;
        }
        categories = categoryService.getCategories();
        long from = 0;
        long to = 0;
        int cateId = Integer.parseInt(categoryChoose);
        if (fromDate != null) {
            from = fromDate.getTime();
        }
        if (toDate != null) {
            to = toDate.getTime();
        }
        posts = postService.searchPost(keyWord, from, to,cateId);
        postViewer = PostViewer.instance.getListPostViewer(posts);
        return SUCCESS;
    }

    @Override
    public void validate() {
        if (isFirst) {
            return;
        }
//        if(fromDate == null && toDate == null && keyWord.equalsIgnoreCase("")){
//            addFieldError("invInput", "Vui lòng nhập thông tin cần tìm kiếm");
//        }
//        if(fromDate != null && toDate != null){
//            if(fromDate.getTime() > toDate.getTime())
//            addFieldError("invToDate", "Ngày không hợp lệ");
//        }
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getOderBy() {
        return oderBy;
    }

    public void setOderBy(String oderBy) {
        this.oderBy = oderBy;
    }

    public List<KeyValue> getListOrderBy() {
        return listOrderBy;
    }

    public void setListOrderBy(List<KeyValue> listOrderBy) {
        this.listOrderBy = listOrderBy;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getCategoryChoose() {
        return categoryChoose;
    }

    public void setCategoryChoose(String categoryChoose) {
        this.categoryChoose = categoryChoose;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public CategoryBusiness getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(CategoryBusiness categoryService) {
        this.categoryService = categoryService;
    }

    public PostBusiness getPostService() {
        return postService;
    }

    public void setPostService(PostBusiness postService) {
        this.postService = postService;
    }

    public static boolean isIsFirst() {
        return isFirst;
    }

    public static void setIsFirst(boolean isFirst) {
        SearchPageAction.isFirst = isFirst;
    }

    public List<KeyValue> getListCategoryKeyValue() {
        return listCategoryKeyValue;
    }

    public void setListCategoryKeyValue(List<KeyValue> listCategoryKeyValue) {
        this.listCategoryKeyValue = listCategoryKeyValue;
    }

    public List<PostViewer> getPostViewer() {
        return postViewer;
    }

    public void setPostViewer(List<PostViewer> postViewer) {
        this.postViewer = postViewer;
    }
    
}
